Hi ! Just letting you know that I've been working for some time on
structuring my dotfiles and finally published them to
`github <https://github.com/cyplo/dotfiles>`__. These contain e.g. my
`vimrc <https://github.com/cyplo/dotfiles/blob/master/.vimrc>`__ and
`font <https://github.com/cyplo/dotfiles/tree/master/.local/share/fonts>`__
`configs <https://github.com/cyplo/dotfiles/tree/master/.config/fontconfig/conf.d>`__.
I've even `patched <https://github.com/cyplo/vim-colors-solarized>`__
the famous `solarized <http://ethanschoonover.com/solarized>`__ theme
for vim to allow better
`gitgutter <https://github.com/airblade/vim-gitgutter>`__ symbols
display. Enjoy !
