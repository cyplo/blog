I have always been fascinated by works of Adrian Chmielarz, from the
times of Metropolis Software on. Naturally then, I am following really
closely on the development of
`Bulletstorm <http://www.bulletstorm.com/>`__. Damn, I even hoped to
talk to The Guy or Mieszko Zielinski at the
`gameaiconf <http://gameaiconf.com/>`__ in Paris, but
`Alex <http://aigamedev.com/>`__ told me that "whoa,
`PCF <http://www.peoplecanfly.com/>`__ is so busy with the new IP that
they didn't have time to come this year, next year they'll be here,
hopefully".

Having something so deeply embedded in your mind that you're always
thinking of it. While you eat, while you talk, in your sleep. That's the
way I do like to work. Reading about making of Bulletstorm gives me
creeps as I do feel their hyperenthusiasm about the thing, I do feel
that they take this game everywhere with them. To dinner, breakfast,
party. That's the way I do like to work.

Now back on track, an interview which triggered this urge to flush my
thoughts here. `Interview with Tanya
Jessen <http://techland.com/2010/10/14/origins-tanya-jessen-lead-producer-on-bulletstorm/>`__,
producer at Epic.

People make games so other people can have fun. How selfless.
