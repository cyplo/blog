After 2 months of waiting - my CNC router arrives. 8 weeks lead time
they said - 7 weeks and 4 days it was ! Who are they ?
`TanieCNC <http://tanie-cnc.pl/>`__ people [CheapCNC in Polish :].
Although it may look like they don't know how to make websites AND their
name does not instill a lot of confidence - but girl, they certainly
know how to weld and make precise machinery ! The size of the package
caught me off guard, I've spent an hour disassembling the crate in full
sun. After that I wasn't able to get it through the stairs myself,
fortunately a friendly neighbour gave me their pair of hands. Lifting
the machine by 2 people is okay, it's still not lightweight, but
bearable. Putting it on the table was a different affair entirely.
Careful not to damage anything, especially the motor assemblies - we've
put it on a improptu wood ramp. Using heavy duty straps, we've lifted it
up little by little. Then some inspection - the quality is really
superb, especially of the metal frame ! After that I got an old PC with
Windows XP and parallel port running Mach3 software - I wanted to set it
up as in any other shop at start. Later on I'm planning on moving to
LinuxCNC and then gradually off parallel port on to a USB stack,
something more like an arduino parsing gcode and driving motors instead
of relying of the accurate timing of the PC. TODOs:

-  add an MDF bed layer on top of existing bed
-  get better clamps
-  get more router bits
-  get a vacuum attachment for the spindle
-  move to LinuxCNC
-  move off parallel-port driving

|image0| |image1| |image2| |image3| |image4| |image5| |image6| |image7|
|image8| |image9| |image10| |image11| |image12| |image13| |image14|
|image15| |image16| |image17| |image18| |image19| |image20| |image21|
|image22|

.. |image0| image:: /wp-content/uploads/2015/04/IMG_20150424_160003-e1430045532334-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_20150424_160003-e1430045532334.jpg
.. |image1| image:: /wp-content/uploads/2015/04/IMG_20150424_161735-e1430045547777-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_20150424_161735-e1430045547777.jpg
.. |image2| image:: /wp-content/uploads/2015/04/IMG_1254-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1254.jpg
.. |image3| image:: /wp-content/uploads/2015/04/IMG_1255-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1255.jpg
.. |image4| image:: /wp-content/uploads/2015/04/IMG_1257-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1257.jpg
.. |image5| image:: /wp-content/uploads/2015/04/IMG_1261-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1261.jpg
.. |image6| image:: /wp-content/uploads/2015/04/IMG_1262-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1262.jpg
.. |image7| image:: /wp-content/uploads/2015/04/IMG_1263-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1263.jpg
.. |image8| image:: /wp-content/uploads/2015/04/IMG_1264-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1264.jpg
.. |image9| image:: /wp-content/uploads/2015/04/IMG_1266-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1266.jpg
.. |image10| image:: /wp-content/uploads/2015/04/IMG_1267-e1430045605467-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1267-e1430045605467.jpg
.. |image11| image:: /wp-content/uploads/2015/04/IMG_1268-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1268.jpg
.. |image12| image:: /wp-content/uploads/2015/04/IMG_1270-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1270.jpg
.. |image13| image:: /wp-content/uploads/2015/04/IMG_1272-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1272.jpg
.. |image14| image:: /wp-content/uploads/2015/04/IMG_1273-e1430045590746-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1273-e1430045590746.jpg
.. |image15| image:: /wp-content/uploads/2015/04/IMG_1274-e1430045623221-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1274-e1430045623221.jpg
.. |image16| image:: /wp-content/uploads/2015/04/IMG_1276-e1430045638798-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1276-e1430045638798.jpg
.. |image17| image:: /wp-content/uploads/2015/04/IMG_1277-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1277.jpg
.. |image18| image:: /wp-content/uploads/2015/04/IMG_1278-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1278.jpg
.. |image19| image:: /wp-content/uploads/2015/04/IMG_1282-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1282.jpg
.. |image20| image:: /wp-content/uploads/2015/04/IMG_1279-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1279.jpg
.. |image21| image:: /wp-content/uploads/2015/04/IMG_1280-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1280.jpg
.. |image22| image:: /wp-content/uploads/2015/04/IMG_1281-150x150.jpg
   :target: /wp-content/uploads/2015/04/IMG_1281.jpg

