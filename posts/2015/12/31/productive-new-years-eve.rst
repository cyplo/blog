It turns out that the last day of the year can be quite productive. I've
managed to sneak in a PCB layout for the input measurement and
protection board for my `sunpowered
server <https://blog.cyplo.net/2015/05/02/grafana-influx/>`__ . Got it
ordered, can't wait to test it ! I got carried away with via stitching
there a bit - we'll see how hard it will be to solder the larger
components because of that. The whole project is hosted
`here <https://lab.cyplo.net/cyplo/sunpowered/tree/master>`__ - it's
open hardware so please do take a look. 

|image0| |image1| |image2|

.. |image0| image::  /wp-content/uploads/2015/12/angled-150x150.png
   :target: /wp-content/uploads/2015/12/angled.png
.. |image1| image:: /wp-content/uploads/2015/12/bottom-150x150.png
   :target: /wp-content/uploads/2015/12/bottom.png
.. |image2| image:: /wp-content/uploads/2015/12/top-150x150.png
   :target: /wp-content/uploads/2015/12/top.png

