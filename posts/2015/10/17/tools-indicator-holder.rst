This is the second part in the series of the tools I use. Tools that are
surprisingly useful, tools that are not that obvious to find. Check out
the first part
`here <https://blog.cyplo.net/2015/03/28/tools-pcb-holder/>`__. Today:
how to calibrate the CNC axis without actually cutting anything ? Use a
test indicator ! How to hold the meter steady though, ? Attach it to the
frame of your router using the power of magnets ! Sample item on Amazon
`here <http://www.amazon.com/gp/product/B00OZA71H6/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B00OZA71H6&linkCode=as2&tag=adventucomput-20&linkId=6ZJKCJHMA2YON4LY>`__
[affiliate link warning] 

|image0| |image1| |image2|

Despite being attached to the frame by its back instead of the bottom it
still holds beautifully.

.. |image0| image:: /wp-content/uploads/2015/10/IMG_1389-150x150.jpg
   :target: /wp-content/uploads/2015/10/IMG_1389.jpg

.. |image1| image:: /wp-content/uploads/2015/10/IMG_1391-150x150.jpg
   :target: /wp-content/uploads/2015/10/IMG_1391.jpg

.. |image2| image:: /wp-content/uploads/2015/10/IMG_1390-150x150.jpg
   :target: /wp-content/uploads/2015/10/IMG_1390.jpg
