It was some time since the last photo-story so, please accept these
pictures of my standing desk. On the actual desk, there is a laptop
stand serving a role of a keyboard and mouse rest. Laptop itself is
flipped on its back, motherboard attached to the back of what once was a
lid. The whole thing is flying on standard monitor desk mount, using
custom vesa-to-acrylic mounting system ;) 

|image0| |image1| |image2| |image3| |image4| |image5| |image6| |image7|

.. |image0| image::  /wp-content/uploads/2015/01/IMG_1238-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1238.jpg
.. |image1| image:: /wp-content/uploads/2015/01/IMG_1239-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1239.jpg
.. |image2| image:: /wp-content/uploads/2015/01/IMG_1240-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1240.jpg
.. |image3| image:: /wp-content/uploads/2015/01/IMG_1241-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1241.jpg
.. |image4| image:: /wp-content/uploads/2015/01/IMG_1242-e1420549865889-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1242-e1420549865889.jpg
.. |image5| image:: /wp-content/uploads/2015/01/IMG_1243-e1420549912666-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1243-e1420549912666.jpg
.. |image6| image:: /wp-content/uploads/2015/01/IMG_1244-e1420549957758-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1244-e1420549957758.jpg
.. |image7| image:: /wp-content/uploads/2015/01/IMG_1246-150x150.jpg
   :target: /wp-content/uploads/2015/01/IMG_1246.jpg
