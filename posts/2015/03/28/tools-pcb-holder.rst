I thought it would be cool to share with you the tools I find
surprisingly useful. Behold the first in the series: the PCB holder ! I
cannot overstate how much is that of a difference from the 'third
hand'-type of holders. The grip is very firm but won't scratch the
surface nor short anything because the jaws are made from a soft
plastic. And the whole thing **ROTATES** ! 

|image0| |image1| |image2|

.. |image0| image:: /wp-content/uploads/2015/03/IMG_1248-150x150.jpg
   :target: /wp-content/uploads/2015/03/IMG_1248.jpg
.. |image1| image:: /wp-content/uploads/2015/03/IMG_1250-150x150.jpg
   :target: /wp-content/uploads/2015/03/IMG_1250.jpg
.. |image2| image:: /wp-content/uploads/2015/03/IMG_1249-150x150.jpg
   :target: /wp-content/uploads/2015/03/IMG_1249.jpg
