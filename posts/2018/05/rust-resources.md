.. title: Resources for starting your adventure with Rust
.. slug: rust-resources
.. date: 2018-05-22 00:00:00 UTC
.. tags: rust, programming, learning
.. category: 
.. link: 
.. description: A list of resources for learning Rust for different learning styles.
.. type: text

As I've been running several intro to Rust sessions throughout the last year, I've assembled a set of resources that help people ease into the language.  

Depending on your learning style you might like:

[Rustlings](https://github.com/rustlings/rustlings) - This is a good set of starter exercises if you want to have a feeling for the language - have links to relevant book sections for each exercises so you can either start with the book or trying to figure it out yourself first. Ah, and it uses the Playground, which means you don't need to install anything on your machine to start.  
[The book itself](https://doc.rust-lang.org/stable/book/second-edition/index.html) - Second edition. Good when you want a solid baseline understanding of the language first.  
[Rust by example](https://doc.rust-lang.org/stable/rust-by-example/) - An set of examples that are runnable within a browser, intertwined with explanatory prose.  
[Exercism](http://exercism.io/)’s Rust exercises - a CLI app that guides you through exercises of increasing difficulty.  
[IntoRust](http://intorust.com/) - A set of short screencasts for the foundational topics.  

Make sure to stay up to date with:

[This week in Rust](https://this-week-in-rust.org/)  
[Awesome Rust](https://rust.libhunt.com/)  
  
And [contribute back](https://www.rustaceans.org/findwork/starters) !  

Don't forget to join the [user forums](http://users.rust-lang.org/) for the warm welcome.

Finally, if you'd like someone to ask questions to or pair program with, [book some time with me](https://calendly.com/cyplo/pair-programming/). 