.. title: I’m running Rust pair programming sessions ! 
.. slug: pair-with-me-on-rust
.. date: 2018-05-20 00:00:00 UTC
.. tags: pair programming, community, tdd, rust, teaching
.. category: 
.. link: 
.. description: I’m running remote and local Rust pair programming sessions ! 
.. type: text

Why ? Rust has such a wonderful community and I want to give back as much as I can.  
I am not an expert in Rust but I am not a beginner either. In addition to that I love pair programming !  
The result is always much better than I could produce myself. I am happy to both share the knowledge and learn.  

I would love to pair with you !  
If you’re a new Rustacean, fresh to the language - come on in ! If you’re an expert - welcome !  

We can work on any of the following:

* Any project of yours !
* Contribute back to a larger open source project (I am a contributor to e.g. [cargo](https://github.com/rust-lang/cargo/), [rustc](https://github.com/rust-lang/rust/) and [rustup](https://github.com/rust-lang-nursery/rustup.rs/))
* A project of mine - e.g. [genpass](https://github.com/cyplo/genpass)

[Click here](https://calendly.com/cyplo/pair-programming/) or ping me an [email](mailto:pair@cyplo.net) to schedule a session - can be a remote one or in person somewhere in London.

Thank you !
