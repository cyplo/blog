Today's story is a photostory ! Here's how to disassemble Pioneer SE-50
headphones, as I found this task quite difficult, mainly because no info
available on the internet on how to open the can from the back. As you
can see Pioneer signs on the side of each can are removable. Take them
off to uncover the screws. Research made possible by
`kabanosy <http://en.wikipedia.org/wiki/Kabanos>`__ - best multitool
ever. Some more info on the headphones: `Manual scan </wp-content/uploads/2011/12/se-50-b.pdf>`__
Produced between '68 and '72 Two speakers per can ! Photos taken after
replacing the cable and before pots rejuvenation. 

`See the gallery`_

.. _See the gallery: /galleries/PioneerSE50/index.html

