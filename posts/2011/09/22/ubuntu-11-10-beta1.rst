| What do I think ? Easy to install, hard to disable Unity, some
  programs crash sometimes and some things are weird. Despite that I
  have the faith in the Ubuntu guys that the system would be ready when
  11.10 comes.
| As whining itself never got anybody anywhere, I've reported all of
  these here:

| `#855901 <https://bugs.launchpad.net/ubuntu/+source/linux/+bug/855901>`__
| `#834425 <https://bugs.launchpad.net/ubuntu/+source/software-center/+bug/834425>`__
| `#855945 <https://bugs.launchpad.net/ubuntu/+source/unity-2d/+bug/855945>`__
| `#855917 <https://bugs.launchpad.net/ubuntu/+source/empathy/+bug/855917>`__
| `#855919 <https://bugs.launchpad.net/ubuntu/+source/thunderbird/+bug/855919>`__

I've even got one OOPS. However, since I'm writing this post under
11.10, it seems usable, even the OOPS was not of disturbance as it got
nice GUI window displayed and no system crash then.
