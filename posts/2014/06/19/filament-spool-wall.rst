I find it useful to hang as much stuff as possible on the walls, instead
of using shelf/floor/desk space for that. Here is just a quick hack I
did to allow filament spools to be mounted securely to a wall. It turns
out that if you cut off the bent part the rod it fits perfectly into the
spool's handle hole. 

|image0| |image1| |image2| |image3| |image4|

.. |image0| image:: /wp-content/uploads/2014/06/IMG_1074-150x150.jpg
   :target: /wp-content/uploads/2014/06/IMG_1074.jpg
.. |image1| image:: /wp-content/uploads/2014/06/IMG_1073-150x150.jpg
   :target: /wp-content/uploads/2014/06/IMG_1073.jpg
.. |image2| image::  /wp-content/uploads/2014/06/IMG_1072-e1403175701941-150x150.jpg
   :target: /wp-content/uploads/2014/06/IMG_1072-e1403175701941.jpg
.. |image3| image:: /wp-content/uploads/2014/06/IMG_1071-150x150.jpg
   :target: /wp-content/uploads/2014/06/IMG_1071.jpg
.. |image4| image:: /wp-content/uploads/2014/06/IMG_1075-e1403175502745-150x150.jpg
   :target: /wp-content/uploads/2014/06/IMG_1075-e1403175502745.jpg
