It's not common for me to put just one link here as the resource for the
article. Today, however, is one of these days. The
`bettercrypto <https://bettercrypto.org/>`__ gals and guys came up with
really excellent practical guide to securing your servers. Here it is:
https://bettercrypto.org/static/applied-crypto-hardening.pdf A draft for
now, but already sporting a high concentration of knowledge. BTW - it
being a draft means that you should read it and contribute to it as
well.
