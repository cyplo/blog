I gave a talk this Monday, an important one I think. The one from the
kind of spreading  knowledge on the safe internet usage to people not
necessarily of the tech background. This was my first one given to a
such audience and to add to it all, it was given in Polish. The biggest
challenge ? Finding good equivalent for the English tech terms. I think
the talks went quite okay and the discussion afterwards was quite
lively. I said a bit on how the internet works and what's wrong with
that, to transition later to what problems Tor addresses and which it
does not. I tried to emphasize that using Tor does not make you
automatically immune to the dangers of the internet. Big thanks to the
organizers, `Praxis <http://praxis.ue.wroc.pl/>`__ student group from
the Wroclaw University of Economy. You can find my slides' sources
`here <https://github.com/cyplo/talks/tree/master/tor_for_beginners>`__,
along with speaker notes.
