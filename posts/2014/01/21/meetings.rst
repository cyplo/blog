Decisions
---------

Most of the meetings come from the desire to have a decision made. The
problem is in most cases is that these are not decisions to be made now.
Software prototyping is cheap. We should just try to build a working
solution and iterate around. Let's prototype. Get someone most annoyed
by the problem and leave them to build it. Of course, the clearer
communication of what they are actually doing the better. It should be
something like ‘hey I’m gonna build this – okay’ or even ‘hey, I”ve
build that, let’s see how it behaves’ Not like “we should now spend
multiple meetings on discussing how this should be done’. Just make it
work, instead of talking about it.

Documentation
-------------

There are people that like those meetings around decision making. And
those decisions, that should not be made now, of course, need proper
documentation around to prove the point. And document the
decision-making process. Oh, and office software and document versions
of course. That's what everyone uses. What, you developers have text
files and this git stuff ?! The thing is if the need for some
documentation for something comes from the team then the team will make
it when needed. If not then probably not.

Sharing the knowledge
---------------------

Other possible reasoning behind having a meeting can be that of some
knowledge needs to be shared. And that's a noble cause. Just don't make
a meeting out of it. Make a lecture. A presentation. No audience members
interacting with each other. Speaker talking and maybe sometimes
allowing questions. The knowledge sharing sessions are oftentimes a
prelude to the decisionmaking meetings. See above.

Confirming your ideas
---------------------

Sometimes however somebody just wants some confirmation on their idea,
maybe before building a prototype. Then, there is a good chance that
they already know who they should ask. No meeting then. Just ask the
people you know you should ask. 1-on-1 interaction. Maybe somebody will
overhear and start listening. Notice that the social dynamic is very
different from the meeting then, two people having a conversation and
another one politely listening, maybe being invited to the conversation
after some while. Just look how it works in between talks on
conferences. Very different from “everybody says everything” meetings.

The meetings that are left
--------------------------

Also, if for some cosmic reason you really need to have a meeting – make
it opt-in. Just the people who are interested coming. Set the timer.
There is one I particularly like - a clock showing amount of money
wasted so far by this meeting.

Post scriptum
-------------

37 signals on meetings:
`http://gettingreal.37signals.com/ch07\_Meetings\_Are\_Toxic.php <https://emeamail.infusion.com/owa/redir.aspx?C=GHIP5YF7-Ue3AtGX8FaxkQmR1qki6NAI6jelfyclFt3i89MSQOQacIq26I2scmQ0qi3zcPAGWZQ.&URL=http%3a%2f%2fgettingreal.37signals.com%2fch07_Meetings_Are_Toxic.php>`__
These guys have the idea of every communication should be async and read
when convenient, hence their emphasis on email. That gets you to really
think of your proposal and really describe it and stuff, which is
sometimes good. To stop and think, RFC-style. However, as mentioned
above, imho most of the times it’s quicker to just write the software.
Possibly, also, I just like ‘hey, got a second?’ approach better.
