I love cooking and well made utensils. That is a part of the
solution. And the problem ? Boosting WiFi signal. Get WiFi USB dongle
and put it where the bowl's focal point is. Or near it, wherever you get
strongest signal boost. I get up to 2x stronger reception with the
antenna presented.

|image0|

.. |image0| image:: https://blog.cyplo.net/wp-content/uploads/2012/08/IMG_0230-300x225.jpg
   :class: alignleft size-medium wp-image-867
   :width: 300px
   :height: 225px
   :target: https://blog.cyplo.net/wp-content/uploads/2012/08/IMG_0230.jpg
