| Try fixing your things. It's already broken - no need to sweat. 
| You might learn something. 
| Level 2 - push for things to be easily fixable.
| It's good for you. It's good for the planet. 

|image0| |image1| |image2| |image3| |image4|

.. |image0| image:: /wp-content/uploads/2016/06/IMG_20160612_120814-150x150.jpg
   :target: /wp-content/uploads/2016/06/IMG_20160612_120814.jpg
.. |image1| image:: /wp-content/uploads/2016/06/IMG_20160612_121012-150x150.jpg
   :target: /wp-content/uploads/2016/06/IMG_20160612_121012.jpg
.. |image2| image:: /wp-content/uploads/2016/06/IMG_20160612_120945-150x150.jpg
   :target: /wp-content/uploads/2016/06/IMG_20160612_120945.jpg
.. |image3| image:: /wp-content/uploads/2016/06/IMG_20160612_120730-150x150.jpg
   :target: /wp-content/uploads/2016/06/IMG_20160612_120730.jpg
.. |image4| image:: /wp-content/uploads/2016/06/IMG_20160612_120723-150x150.jpg
   :target: /wp-content/uploads/2016/06/IMG_20160612_120723.jpg
