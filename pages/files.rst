I try to mirror some rare gems here, to lower their chance of getting
extinct on the net.

PoC \|\| GTFO 
^^^^^^^^^^^^^

| SHA256 (`pocorgtfo00.pdf <pocorgtfo/pocorgtfo00.pdf>`_) = c4d1d1091187b98a9bb28452bc6564a1e8c0ce10d20ba2b4a20f8b7798f7ab64
| SHA256 (`pocorgtfo01.pdf <pocorgtfo/pocorgtfo01.pdf>`_) = a0f93a265d38257a06fd7fd210f73ea9c55a94ac1305c65c0510ada236c2cc88
| SHA256 (`pocorgtfo02.pdf <pocorgtfo/pocorgtfo02.pdf>`_) = f427e8d95c0ac15abe61d96fb75cfb55df1fd5ac9e713cf968f3602267ca155e
| SHA256 (`pocorgtfo03.pdf <pocorgtfo/pocorgtfo03.pdf>`_) = 7094f5c6a3936e0d0b8f5e42b4d1940413f568e9a3617be0d7d6dc73cb3420e1
| SHA256 (`pocorgtfo04.pdf <pocorgtfo/pocorgtfo04.pdf>`_) = 1d1567b8ac533cd142a8af560266ca60939fed02e3af1f6fd0816b26473afd01
| SHA256 (`pocorgtfo05.pdf <pocorgtfo/pocorgtfo05.pdf>`_) = 9623609a9c0ecd95674e6da3de322baa141f5460cbcb93eeaade22eaf2c80640
| SHA256 (`pocorgtfo06.pdf <pocorgtfo/pocorgtfo06.pdf>`_) = bf4d8846fbbb1071c7ec033004eda8ea8809676fe388db6faa020d781cb8ac26
| SHA256 (`pocorgtfo07.pdf <pocorgtfo/pocorgtfo07.pdf>`_) = 601534f4355c5e0eb292c6dd6edaf5055625d23e0de869f88193606415e6a35f
| SHA256 (`pocorgtfo08.pdf <pocorgtfo/pocorgtfo08.pdf>`_) = 7a942c425f471f99d8cba8da117cc4a53cddb3551e4b16c8b9feae31b5654a33
| SHA256 (`pocorgtfo09.pdf <pocorgtfo/pocorgtfo09.pdf>`_) = 8ad70d4dd0c0f53e8c479d1d573e5a365ea673acafa9fd61fa5231e18502a6ad
| SHA256 (`pocorgtfo10.pdf <pocorgtfo/pocorgtfo10.pdf>`_) = 1e350e30383fd332678654b6067fe4b6ea3d25d7f41a24a4c81fe913b295c9de
| SHA256 (`pocorgtfo11.pdf <pocorgtfo/pocorgtfo11.pdf>`_) = 44d56d717c7b3baf7e11aa6624d5a80a90b132a519e61b9682a5f4a635b04c78
| SHA256 (`pocorgtfo12.pdf <pocorgtfo/pocorgtfo12.pdf>`_) = 441216e475e69564192f2121daa5dd465835072718366b75b08b9272ff9cf08b
| SHA256 (`pocorgtfo13.pdf <pocorgtfo/pocorgtfo13.pdf>`_) = c881c67557af52864654791a2a494f329a2fa397236bf0e961508f0769b0a3f5
| SHA256 (`pocorgtfo14.pdf <pocorgtfo/pocorgtfo14.pdf>`_) = b9db617dcc146cc99f4379b3162a35818d884bf4032ab854b6ec00b5ec98138d
| SHA256 (`pocorgtfo15.pdf <pocorgtfo/pocorgtfo15.pdf>`_) = c9b3f5026640efae12d75e62868931e2b2b5ad98a9b858408266ac5c35815bf4
| SHA256 (`pocorgtfo16.pdf <pocorgtfo/pocorgtfo16.pdf>`_) = 10f0cb977f03824737a413079ded14b237b7ee155a5397e804586ab7151ed0a3
| SHA256 (`pocorgtfo17.pdf <pocorgtfo/pocorgtfo17.pdf>`_) = 40b8985521e671b59c305d2f5512f31b95f1e8c59b9c05ad2ca6413a99d59c97
| SHA256 (`pocorgtfo18.pdf <pocorgtfo/pocorgtfo18.pdf>`_) = cd1e5a8f9fdda3a950556ad77db706ab8a01c97662e79c46d417e0e7858aa742 (or f427637767d7aea764e0f2045ea4de86bbf5410a43535e70285add347047b283)

